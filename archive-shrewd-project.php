<?php
/**
 * The Template for displaying all single projects.
 */

get_header(); ?>

	<?php
	$args = array( 'post_type' => 'shrewd-project', 'posts_per_page' => 8 );
	$loop = new WP_Query( $args );
	?>

	<div id="primary" class="site-content">
		<div id="content" role="main">

			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

				<?php get_template_part( 'title', get_post_format() ); ?>

			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
