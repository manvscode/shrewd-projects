<?php
/*
	Plugin Name: Shrewd Projects
	Plugin URI: http://www.manvscode.com/
	Description: Display your projects from Wordpress.
	Version: 0.1
	Author: Joe Marrero
	Author URI: http://www.manvscode.com
	License: GPL2

	Copyright 2012  Joe Marrero  (email: manvscode@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
//require_once dirname(__FILE__) . '/utility.php';


add_action( 'init', 'create_post_type' );
add_action( 'pre_get_posts', 'limit_projects_per_page' );

function create_post_type()
{
	register_post_type( 'shrewd-project', array(
		'labels' => array(
			'name'          => __( 'Projects' ),
			'singular_name' => __( 'Project' )
		),
		'public' => true,
		'has_archive' => true,
		'supports' => array(
			'title',
			'editor',
			'thumbnail',
			'excerpt',
			'comments',
		),
		'query_var' => true,
		'show_ui' => true,
		'menu_icon' => plugins_url() . '/shrewd-projects/project.png',
		'rewrite' => array('slug' => 'projects'),
	));
}

function limit_projects_per_page()
{
	if( get_query_var('post_type') == 'shrewd-project' )
	{
		set_query_var( 'posts_per_archive_page', 8 );
	}
}
