<?php
/**
 * The Template for displaying all single projects.
 */

get_header(); ?>

	<?php
	$args = array( 'post_type' => 'shrewd-project', 'posts_per_page' => 1 );
	$loop = new WP_Query( $args );
	?>

	<div id="primary" class="site-content">
		<div id="content" role="main">

			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

				<?php get_template_part( 'title', get_post_format() ); ?>
				<div class="entry-content">
				<?php get_template_part( 'content', get_post_format() ); ?>
				</div>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() )
						comments_template( '', true );
				?>

			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
